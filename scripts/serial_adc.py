#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2014, Grupo de Mecatronica USB.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of Willow Garage, Inc. nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Revision $Id$

## Demoqe128 serial program

import rospy
import sys, getopt
import serial
import struct
from std_msgs.msg import UInt16MultiArray

def usage():
  print 'Usage: '+sys.argv[0]+' -p <SerialPort>'

def ros_serial_demoqe128():
    Port = '/dev/ttyUSB0'

    try:
	opts, args = getopt.getopt(sys.argv[1:], "p:")
    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    
    for o, a in opts:
	if o == "-p":
		Port = a
	else:
		assert False, "unhandled option"	


    try:
    	ser = serial.Serial(Port, 115200)
    except serial.SerialException:
	print "No connection to the device could be established"
	usage()
        sys.exit(2)



    pub = rospy.Publisher('/adc_channels', UInt16MultiArray)
    rospy.init_node('serial_adc', anonymous=False)
    r = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        #ser.flushInput()
	if ser.inWaiting()>=9:
		while ser.inWaiting()>8:
		        line = ser.readline()
			#print map(ord,line)
			if len(line) == 9:
				adc_channels = struct.unpack('>HHHH',line[0:-1])
				#print adc_channels, type(adc_channels[0]) 
				rospy.loginfo("ADC data from DEMOQE128: %d"%adc_channels[0])
				msg = UInt16MultiArray(None, adc_channels)
				pub.publish(msg)
			else:
				print rospy.logerr("Wrong package size: %d"%len(line))
        r.sleep()
        
if __name__ == '__main__':
    try:
        ros_serial_demoqe128()
    except rospy.ROSInterruptException: pass
