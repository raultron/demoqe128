# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Summary of set up:

* Install ros groovy (maybe it will work with other versions).
* Program the DEMOQE128 using the project that can be found in root_folder/Codewarrior.
* Connect the signals to pins PTA0 - PTA3 (PTA0 is the potentiometer).
* Connect the DEMOQE128 to the computer using a serial cable.
* Execute in one terminal: roscore
* and in another terminal: rosrun demoqe128 serial_adc.py

Check the permissions of your Serial or USB/Serial device:

* ls -l /dev/ttyS0
* sudo chmod a+rw /dev/ttyS0

* ls -l /dev/ttyUSB0
* sudo chmod a+rw /dev/ttyUSB0

Topics and messages:

* The ADC values will be published in the topic: /adc_channels.
* The Topic message definition is: std_msgs/UInt16MultiArray

How to run tests:

In order to plot the value of one channel use the following command:
rqt_plot /adc_channels/data[0]
