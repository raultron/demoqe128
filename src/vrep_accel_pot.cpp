#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <ros/ros.h>
#include "std_msgs/Float32.h"
#include "std_msgs/Int16MultiArray.h"
#include "std_msgs/UInt16.h"
#define ESCALA_ROT 0.01
#define ESCALA_Z   0.1/65536
#define MEDIA_Z    0.1/2


double height   = 0;
double roll     = 0;
double pitch    = 0;
double gravity  = 0;

void accelCallback(const std_msgs::Int16MultiArrayConstPtr& in)
{
	roll      = double(in->data[0])*ESCALA_ROT;
        pitch     = double(in->data[1])*ESCALA_ROT;
        gravity   = double(in->data[2]);
	
}

void potCallback(const std_msgs::UInt16ConstPtr& in)
{
	height = double(in->data)*ESCALA_Z - MEDIA_Z;
	
}

int main(int argc,char* argv[])
{
    ros::init(argc, argv, "vrep_accel_pot"); // Nombre del nodo
    ros::NodeHandle n;

    ros::Subscriber accel_sub      = n.subscribe("/accel",1,accelCallback);
    ros::Subscriber pot_sub        = n.subscribe("/pot",1,potCallback);

    ros::Publisher  height_pub     = n.advertise<std_msgs::Float32>("/cmd_height", 1);
    ros::Publisher  roll_pub       = n.advertise<std_msgs::Float32>("/cmd_roll", 1);
    ros::Publisher  pitch_pub      = n.advertise<std_msgs::Float32>("/cmd_pitch", 1);
    ros::Publisher  gravity_pub    = n.advertise<std_msgs::Float32>("/cmd_gravity", 1);

    std_msgs::Float32 out_height;
    std_msgs::Float32 out_roll;
    std_msgs::Float32 out_pitch;
    std_msgs::Float32 out_gravity;

    while(n.ok()){
        ros::spinOnce();

        out_height.data  = height;
	out_roll.data    = roll;
        out_pitch.data   = pitch;
	out_gravity.data = gravity;

        height_pub.publish(out_height);
	roll_pub.publish(out_roll);
	pitch_pub.publish(out_pitch);
	gravity_pub.publish(out_gravity);

        usleep(5000);
        }
      
}

