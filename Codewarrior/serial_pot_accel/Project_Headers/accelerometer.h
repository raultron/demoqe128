/*
 * accelerometer.h
 *
 *  Created on: May 31, 2014
 *      Author: tron
 */

#ifndef ACCELEROMETER_H_
#define ACCELEROMETER_H_


byte Accel_Configuration(void);
byte Accel_Read(byte* Values);
signed char Convert_Accel_Value(unsigned char Value);




#endif /* ACCELEROMETER_H_ */
