/** ###################################################################
**     Filename    : ProcessorExpert.c
**     Project     : ProcessorExpert
**     Processor   : MC9S08QE128CLK
**     Version     : Driver 01.12
**     Compiler    : CodeWarrior HCS08 C Compiler
**     Date/Time   : 2014-05-31, 19:19, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/* MODULE ProcessorExpert */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "Events.h"
#include "EI2C1.h"
#include "Inhr1.h"
#include "Inhr2.h"
#include "AS1.h"
/* Include shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"

#define ACCEL_AXIS 3
#define ACCEL_AXIS_DATA_SIZE 1
//We define the Serial buffer with an space for an additional EOL character
#define SERIAL_OUT_SIZE (ACCEL_AXIS*ACCEL_AXIS_DATA_SIZE+1)

unsigned char cSerialOut[SERIAL_OUT_SIZE];

/* User includes (#include below this line is not maintained by Processor Expert) */
#include "accelerometer.h"
void main(void)
{
  /* Write your local variable definition here */
	unsigned char cAccelValues[3],cError;
	word wSent;
  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  PE_low_level_init();
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */
  /* For example: for(;;) { } */
  
  cError=Accel_Configuration();
  
  for(;;){
  //Max Speed sampling
  cError = Accel_Read(cAccelValues);
  
  
  //Prepare everything to send
  cSerialOut[0] = cAccelValues[0];
  cSerialOut[1] = cAccelValues[1];
  cSerialOut[2] = cAccelValues[2];
  cSerialOut[3] = '\n';
  
  //Send the buffer
  //If buffer is full sample again.
  if(AS1_GetCharsInTxBuf()==0)
  	  cError = AS1_SendBlock(cSerialOut,SERIAL_OUT_SIZE,&wSent);
  
  }
  
  
  
  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;){}
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END ProcessorExpert */
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.0 [05.03]
**     for the Freescale HCS08 series of microcontrollers.
**
** ###################################################################
*/
