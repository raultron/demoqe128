################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/accelerometer.c" \

C_SRCS += \
../Sources/accelerometer.c \

OBJS += \
./Sources/accelerometer_c.obj \

OBJS_QUOTED += \
"./Sources/accelerometer_c.obj" \

C_DEPS += \
./Sources/accelerometer_c.d \

C_DEPS_QUOTED += \
"./Sources/accelerometer_c.d" \

OBJS_OS_FORMAT += \
./Sources/accelerometer_c.obj \


# Each subdirectory must supply rules for building sources it contributes
Sources/accelerometer_c.obj: ../Sources/accelerometer.c
	@echo 'Building file: $<'
	@echo 'Executing target #1 $<'
	@echo 'Invoking: HCS08 Compiler'
	"$(HC08ToolsEnv)/chc08" -ArgFile"Sources/accelerometer.args" -ObjN="Sources/accelerometer_c.obj" "$<" -Lm="$(@:%.obj=%.d)" -LmCfg=xilmou
	@echo 'Finished building: $<'
	@echo ' '

Sources/%.d: ../Sources/%.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '


