/** ###################################################################
**     Filename    : ProcessorExpert.c
**     Project     : ProcessorExpert
**     Processor   : MC9S08QE128CLK
**     Version     : Driver 01.12
**     Compiler    : CodeWarrior HCS08 C Compiler
**     Date/Time   : 2014-02-11, 14:29, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/* MODULE ProcessorExpert */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "Events.h"
#include "TI1.h"
#include "AD1.h"
#include "AS1.h"
/* Include shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"

unsigned char cEstado = ESPERAR;
unsigned char cCodError;
unsigned int iEnviados;

#define ADC_CHANNELS 4
#define SERIAL_OUT_SIZE (ADC_CHANNELS*sizeof(word)+1)

// We define Unions in case it is necessary to access each ADC measure individually (high and low bytes)
typedef union{
	unsigned char u8[2];
	unsigned int u16;
}Single_ADC_Value;

// We define the array of ADC measurements
// We give this union a convenient byte array pointer.
typedef union{
	Single_ADC_Value sADC[ADC_CHANNELS];
	unsigned char cValues[sizeof(sADC)*ADC_CHANNELS]; //Size 8
}Multiple_ADC_Values;

Multiple_ADC_Values ADC_Out = {0};


unsigned char cSerialOut[SERIAL_OUT_SIZE];
                   

/* User includes (#include below this line is not maintained by Processor Expert) */

void main(void)
{
  /* Write your local variable definition here */
  int i=0;
  iEnviados = 0;

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  PE_low_level_init();
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */
  /* For example: for(;;) { } */
  for(;;){
	  switch (cEstado){
	  case ESPERAR:
		  break;
	  case MEDIR:
		  cCodError = AD1_Measure(TRUE);
		  cCodError = AD1_GetValue16((word *)ADC_Out.cValues);
		  cEstado = ENVIAR;
		  break;
	  case ENVIAR:
		  //Obtain Values from ADC into SerialOut Variable
		  //Also we check if the end of line is present for replacement
		  for (i=0;i<ADC_CHANNELS*2;i++){
			  if (ADC_Out.cValues[i]=='\n'){
				  cSerialOut[i] = ADC_Out.cValues[i]-1;
			  }
			  else{
				  cSerialOut[i] = ADC_Out.cValues[i];
			  }
			  
		  }
		  
		  //We set the End Of line in our buffer
		  cSerialOut[SERIAL_OUT_SIZE-1]= '\n';
		  
		  //Send the buffer
		  if(AS1_GetCharsInTxBuf()==0)
			  cCodError = AS1_SendBlock(cSerialOut,SERIAL_OUT_SIZE,&iEnviados);		  
		  cEstado = ESPERAR;
		  break;
	  default:
		  break;
	  
	  }
	  
  }
  

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;){}
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END ProcessorExpert */
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.0 [05.03]
**     for the Freescale HCS08 series of microcontrollers.
**
** ###################################################################
*/
