/** ###################################################################
**     Filename    : ProcessorExpert.c
**     Project     : ProcessorExpert
**     Processor   : MC9S08QE128CLK
**     Version     : Driver 01.12
**     Compiler    : CodeWarrior HCS08 C Compiler
**     Date/Time   : 2014-05-30, 21:51, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/* MODULE ProcessorExpert */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "Events.h"
#include "IIC_module.h"
/* Include shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"

/* User includes (#include below this line is not maintained by Processor Expert) */
void IIC_configuration (void);
void Master_Read_and_Store(void);
void Master_Transmit(byte transbytes, byte recnum);
void Master_Receive(void);
void MMA7660_configuration(void);
void Master_Read_MMA7660_register(byte transbytes, byte recbytes);

#pragma DATA_SEG _DATA_ZEROPAGE
static byte samp=0,mode=0,n_str[5];
static word StartCount,StopCount;

static byte last_byte = 0,bytes_to_trans = 0, reading_mma7660_reg = 0, repeat_start_sent = 0;
static byte count = 0,rec_count = 0,num_to_rec = 0,mma7660[] = {0,0,0,0};
static word IIC_Rec_Data[ ]={0,0,0,0};

void main(void)
{
  /* Write your local variable definition here */

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  PE_low_level_init();
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */
  /* For example: for(;;) { } */
  //IIC_configuration();
  MMA7660_configuration();
  for(;;){
	  if (PTHD_PTHD7 == 1) {        //Wait for IIC bus to be free
	     while (PTHD_PTHD7 == 0);    // Wait while pin is low
	     while (IIC2C1_MST == 1);    // Wait untill IIC is stopped 
	     //Read Xout, Yout, Zout
	     mma7660[0] = 0x98;
	     mma7660[1] = 0x00;
	     Master_Read_MMA7660_register(2,3); 
      }else{
	        
	  }
  }
	  

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;){}
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END ProcessorExpert */
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.0 [05.03]
**     for the Freescale HCS08 series of microcontrollers.
**
** ###################################################################
*/

/////////////////////////////////////////////////////////////////////////////////////////
//  IIC_configuration
//---------------------------------------------------------------------------------------
//  fIIC is 25MHz/4/48 = 130KHz
//  Enable IIC module and interrupts
/////////////////////////////////////////////////////////////////////////////////////////
void IIC_configuration (void) {
 
  IIC2F = 0x90;         /* Multiply factor of 4. SCL divider of 48 */
  IIC2C1  = 0xC0;       /* Enable IIC module and interrupts */
} 

/////////////////////////////////////////////////////////////////////////////////////////
//  Function used as Master
//---------------------------------------------------------------------------------------
//  Master_Read_and_Store
//  Master_Write_MMA7660_register
//  Master_Read_MMA7660_register
/////////////////////////////////////////////////////////////////////////////////////////

void Master_Read_and_Store(void) {
  IIC_Rec_Data[rec_count++] = IIC2D; 
}


void Master_Write_MMA7660_register(byte transbytes) {
  last_byte = 0;                    // Initialize variables to 0
  count = 0;
  bytes_to_trans = transbytes;       
  
  if (transbytes == 0) return; 
  
  IIC2C1_TX = 1;                    // Set TX bit for Address cycle
  IIC2C1_MST = 1;                   // Set Master Bit to generate a Start
  
  IIC2D = mma7660[count++];         // Send first byte (should be 7-bit address + R/W bit)    
}

void Master_Read_MMA7660_register(byte transbytes, byte recbytes) {

  rec_count = 0;                    // Initialize variables to 0
  last_byte = 0;                    
	count = 0;
	repeat_start_sent = 0;
	
  bytes_to_trans = transbytes;      
  num_to_rec = recbytes;
  
  
  if (transbytes == 0) return;  
    
  IIC2C1_TXAK = 0;
  IIC2C1_TX = 1;                    // Set TX bit for Address cycle
  IIC2C1_MST = 1;                   // Set Master Bit to generate a Start
  
  reading_mma7660_reg = 1;
  IIC2D = mma7660[count++];         // Send first byte (should be 7-bit address + R/W bit)    
}

/////////////////////////////////////////////////////////////////////////////////////////
//  MMA7660_configuration
//---------------------------------------------------------------------------------------
//  MMA7660 uses default configuration
//  120 samples/second; Disable interrupts
//  MMA7660 enter into active mode
/////////////////////////////////////////////////////////////////////////////////////////
void MMA7660_configuration(void){
  
  mma7660[0] = 0x98;
  mma7660[1] = 0x07;
  mma7660[2] = 0x01;
  Master_Write_MMA7660_register(3);
  
}

ISR(IIC_ISR)
{
  IIC2S_IICIF = 1;              // Clear Interrupt Flag   
  if (IIC2C1_TX) {              // Transmit or Receive?                            
///////////////////// Transmit ////////////////////////////					
	if (repeat_start_sent) {
	  IIC2C1_TX = 0;                // Switch to RX mode 
	  if (num_to_rec == 1)
		IIC2C1_TXAK = 1;            // This sets up a NACK  
		  IIC2D;                        // Dummy read from Data Register 
	} 
	else if ((last_byte) & (reading_mma7660_reg)) {
	  IIC2C1_RSTA = 1;              //Repeat start
	  IIC2D = (mma7660[0] | 0x01);  //Set Read bit
	  repeat_start_sent = 1;
	}
		else if (last_byte) {           // Is the Last Byte? 
			IIC2C1_MST = 0;               // Generate a Stop 
		} 
		else if (last_byte != 1) {
		  if (IIC2S_RXAK) {             // Check for ACK 
			  IIC2C1_MST = 0;             // No ACk Generate a Stop 
		  } 
		  else if (!IIC2S_RXAK) {		  
			IIC2D = mma7660[count++];     // Transmit Data   			
			  if (count == bytes_to_trans) 
				last_byte = 1;			    		  
		  }
		}
  } else {			 
///////////////////// Receive ////////////////////////////		 
	if ((num_to_rec - rec_count) == 2) {      
		  IIC2C1_TXAK = 1;          // This sets up a NACK
		  Master_Read_and_Store();		   
	}
	else if ((num_to_rec - rec_count) == 1) {      
		  IIC2C1_MST = 0;           // Send STOP 
		  Master_Read_and_Store();
		}			
		else {		
		Master_Read_and_Store();
		}
  }	
	
}
