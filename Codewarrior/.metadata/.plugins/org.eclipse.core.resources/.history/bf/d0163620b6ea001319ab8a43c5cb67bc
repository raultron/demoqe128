/*
 * accelerometer.c
 *
 *  Created on: May 31, 2014
 *      Author: Ra�l ACu�a
 *  Description:
 *  	Reads accelerometer axis values from the MMA7660 integrated accelerometer of the DEMOQE128 board
 *  	The MMA7660 is connected to the IIC bus off the microcontroller (PTHD6 and PTHD7). 
 *  Usage:
 *  This library requires the configuration of a processor expert module (Software emulated I2C)
 *  Data Pin: PTHD7
 *  Clock Pin: PTHD6
 *  Mode selection: Master
 *  I2C bus mode STANDARD
 *  Automatic stop condicion: no
 *  Slave address init: 4C
 *  
 *  
 */

/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "Events.h"
#include "EI2C1.h"
#include "Inhr1.h"
#include "Inhr2.h"
/* Include shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "accelerometer.h"

//Macro to check if a bit in a variable is '1'
#define TEST_BIT(var,bit) ((var) & (1<<(bit)))

/////////////////////////////////////////////////////////////////////////////////////////
//  AccelConfiguration
//---------------------------------------------------------------------------------------
//  MMA7660 uses default configuration
//  120 samples/second; Disable interrupts
//  MMA7660 enter into active mode
/////////////////////////////////////////////////////////////////////////////////////////
byte Accel_Configuration(void){
	byte DataOut[2];
	byte Error;
	word sent;
	DataOut[0]=0x07;
	DataOut[1]=0x01;  
	Error= EI2C1_SendBlock(&DataOut,2,&sent);
	return Error;
}
/////////////////////////////////////////////////////////////////////////////////////////
//  AccelRead
//---------------------------------------------------------------------------------------
//  Reads the axiss values from the accelerometer
//  Parameter:
//  	byte* Value   Pointer to the array where the variables will be saved.
/////////////////////////////////////////////////////////////////////////////////////////
byte Accel_Read(byte* Values){
  // Consecutive reads on MMA7660
  // 0x00 points to X value, each consecutive read increments pointer.
  byte ErrorCode;
  bool Alert;
  ErrorCode = EI2C1_SendChar(0x00);
  ErrorCode = EI2C1_RecvChar(&Values[0]);
  ErrorCode = EI2C1_RecvChar(&Values[1]);
  ErrorCode = EI2C1_RecvChar(&Values[2]);
  
  //Check for invalid measurements (read manual), check for assertion in sixth bit  
  Alert = TEST_BIT(Values[0],6) | TEST_BIT(Values[1],6) | TEST_BIT(Values[2],6); 
  
  //Convert Values to signed 8 bit format
  Values[0] = Convert_Accel_Value(Values[0]);
  Values[1] = Convert_Accel_Value(Values[1]);
  Values[2] = Convert_Accel_Value(Values[2]);
  
  
  if (Alert)
	  return ERR_VALUE;
  else
	  return ErrorCode;
 }

/////////////////////////////////////////////////////////////////////////////////////////
//  Convert_Accel_Value
//---------------------------------------------------------------------------------------
//  Extends the sign for 8bit.
//  Parameter:
//  	unsigned char Value   5bit signed value from the accelerometer.
/////////////////////////////////////////////////////////////////////////////////////////
signed char Convert_Accel_Value(unsigned char Value){
	if (TEST_BIT(Value,5)){
		//The number is negative
		//Sign extension
		Value = Value | 0xE0;
	}
	return Value	
}
