/** ###################################################################
**     Filename    : ProcessorExpert.c
**     Project     : ProcessorExpert
**     Processor   : MC9S08QE128CLK
**     Version     : Driver 01.12
**     Compiler    : CodeWarrior HCS08 C Compiler
**     Date/Time   : 2014-05-31, 19:19, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/* MODULE ProcessorExpert */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "Events.h"
#include "EI2C1.h"
#include "Inhr1.h"
#include "Inhr2.h"
#include "AS1.h"
#include "TI1.h"
#include "AD1.h"
/* Include shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"

/* User includes (#include below this line is not maintained by Processor Expert) */
#include "accelerometer.h"

#define ACCEL_AXIS 3
#define ACCEL_AXIS_DATA_SIZE 1  //Size of byte
#define ADC_CHANNELS 1
#define ADC_DATA_SIZE 2  //Size of word

//We define the Serial buffer with an space for an additional EOL character
#define SERIAL_OUT_SIZE (ACCEL_AXIS*ACCEL_AXIS_DATA_SIZE+ADC_CHANNELS*ADC_DATA_SIZE+1)

unsigned char cSerialOut[SERIAL_OUT_SIZE];
unsigned char cState = WAIT;

// We define Unions in case it is necessary to access each ADC measure individually (high and low bytes)
typedef union{
	unsigned char u8[2];
	unsigned int u16;
}Single_ADC_Value;
static Single_ADC_Value ADC_Out;


void main(void)
{
  /* Write your local variable definition here */
	unsigned char cAccelValues[3],cError;
	word wSent;
	int i;
  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  PE_low_level_init();
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */
  /* For example: for(;;) { } */
  
  cError=Accel_Configuration();
  
 
  for(;;){
  	  switch (cState){
  	  case WAIT:
  		  break;
  	  case SAMPLE:
  		  //Measure Accelerometer
  		  cError = Accel_Read(cAccelValues);
  		  if (cError != ERR_VALUE)
  			  break;
  		  //Measure Potenciometer
  		  cError = AD1_Measure(TRUE);
  		  cError = AD1_GetValue16((word *)ADC_Out.u16);
  		  cState = SEND;
  		  break;
  	  case SEND:
  		  //Copy Values from ADC and Accelerometer into cSerialOut Variable
  		  //Also we check if the end of line is present for replacement
  		  //Prepare everything to send
  		  cSerialOut[0] = ADC_Out.u8[0];
  		  cSerialOut[1] = ADC_Out.u8[1];
  		  cSerialOut[2] = cAccelValues[0];
  		  cSerialOut[3] = cAccelValues[1];
  		  cSerialOut[4] = cAccelValues[2];
  		  
  		  //Check for EOL char and replacement
  		  for (i=0;i<(SERIAL_OUT_SIZE-2);i++){
  			  if (cSerialOut[i]=='\n'){
  				  cSerialOut[i] = 1; //We replace '\n' with 1
  			  }  			  
  		  }
  		  
  		  //We set the End Of line in our buffer
  		  cSerialOut[SERIAL_OUT_SIZE-1]= '\n';
  		  
  		  
  		  //Send the buffer
  		  if(AS1_GetCharsInTxBuf()==0)
  			  cError = AS1_SendBlock(cSerialOut,SERIAL_OUT_SIZE,&wSent);		  
  		  cState = WAIT;
  		  break;
  	  default:
  		  break;
  	  
  	  }
  	  
    }
  
  
  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;){}
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END ProcessorExpert */
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.0 [05.03]
**     for the Freescale HCS08 series of microcontrollers.
**
** ###################################################################
*/
